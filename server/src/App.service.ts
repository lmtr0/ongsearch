import { Injectable } from "@nestjs/common";
import {  v4 as uuidv4 } from "uuid"


@Injectable()
export class AppService {
    public genID(): string {
        let res: string = uuidv4();
        return res.replace(/(\-)/g, "");
    } 
}