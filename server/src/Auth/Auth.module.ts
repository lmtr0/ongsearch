import { HttpModule } from "@nestjs/axios";
import { Module } from "@nestjs/common";
import { MongooseModule } from "@nestjs/mongoose";
import { AppService } from "../App.service";
import { AuthController } from "./Auth.controller";
import { UserSchema } from "./Auth.model";
import { AuthResolver } from "./Auth.resolver";
import { AuthService } from "./Auth.service";


@Module({
    controllers: [AuthController],
    imports: [
        HttpModule,
        MongooseModule.forFeature([{ name: 'User', schema: UserSchema }]),
    ], 
    providers: [
        AppService,
        AuthService,
        AuthResolver
    ],
    exports: [
        AuthService,
    ]
})
export class AuthModule {}