import { Injectable } from "@nestjs/common";
import { InjectModel } from "@nestjs/mongoose";
import { FilterQuery, Model } from "mongoose";
import { AppService } from "src/App.service";
import { IUser, UserDocument } from "./Auth.model";
import { decode, sign, verify } from "jsonwebtoken";
import { classicNameResolver } from "typescript";

export type ValidationResult = [ boolean, any, IUser? ] 

@Injectable()
export class AuthService {
    constructor(
        @InjectModel('User') private db: Model<UserDocument>,
        private appServ: AppService,
    ){}

    public async insertOne(user: IUser) {
        let doc = new this.db({
            ...user,
            _id: this.appServ.genID(),
            state: `STATE::${this.appServ.genID()}`,
        });

        return await doc.save();
    }

    public async invalidateState(user: IUser) {
        return await this.db.findOneAndReplace({_id: user._id}, { ...user, state: `STATE::${this.appServ.genID()}` });
    }

    public async findOne(filter: FilterQuery<UserDocument>)
    {
        return await this.db.findOne(filter);
    }

    public async findOneById(_id: string)
    {
        return await this.db.findById(_id);
    }

    public async validateAccessToken(accessToken: string): Promise<ValidationResult> {
        let token;
        try {
            token = verify(accessToken, process.env.SECRET) as any;
        } catch { return [true, null, null]; }
        let user = await this.findOneById(token.user_id);
        
        return user.state === token.state ? 
            [ false, token, user] : 
            [true, null, null];
    }

    public async validateRefreshToken(refreshToken: string): Promise<ValidationResult> {
        let token;
        try {
            token = verify(refreshToken, process.env.SECRET) as any;
        } catch { return [true, null, null]; }
        
        let user = await this.db.findById(token.user_id);
        return user.state === token.state ? [
            false,
            token, 
            user,
        ] : [ 
            true,
            null, 
            null, 
        ];
    }

    public async generateRefreshToken(user: UserDocument) {
        let token = sign({
            user_id: user._id,
            state: user.state,
        }, process.env.SECRET, {
            issuer: 'ongsearch-issuer', 
            audience: 'ongsearch-users',
            expiresIn: '7d',
        })

        return token;
    }

    public async generateAccessToken(refreshToken: string) {
        let token;
        try {
            token = verify(refreshToken, process.env.SECRET) as any;
        } catch { return null; }
        
        let user = await this.db.findById(token.user_id);
        
        if (user?.state === token.state)
        {
            return sign({
                user_id: user._id,
                state: user.state
                // as we don't deal with passwords, there is no need to store a state here
            }, process.env.SECRET, {
                issuer: 'ongsearch-issuer', 
                audience: 'ongsearch-users',
                expiresIn: '1d',
            })
        }
        return null;
    }
}