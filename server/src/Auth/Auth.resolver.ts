import { Req } from "@nestjs/common";
import { IUser, UserDocument, UserObjectType } from "./Auth.model";
import { Resolver, Query, Args, Mutation } from "@nestjs/graphql";
import { AuthService } from "./Auth.service";
import { verify } from "jsonwebtoken";


@Resolver()
export class AuthResolver {
    constructor(
        private authServ: AuthService,
    ) {}
    
    @Mutation(() => String, {nullable: true})
    public async createUserAccessToken(@Args('refreshToken') refreshToken: string) {
        return this.authServ.generateAccessToken(refreshToken);
    }

    @Query(() => UserObjectType, {nullable: true})
    public async user( @Args('accessToken') accessToken: string) {
        try {
            const {user_id, state} = verify(accessToken, process.env.SECRET) as any;
            let user = (await this.authServ.findOneById(user_id)) ?? new UserDocument();

            if (user?.state  !== state)
                return null;

            return user;
        }
        catch { return null; }
    }
}