import { HttpService } from "@nestjs/axios";
import { Controller, Get, Req, Res, Request, Query } from "@nestjs/common";
import { CookieOptions } from "express";
import { decode } from "jsonwebtoken"  
import { Authorization, IGoogleUser, IUser, UserDocument } from "./Auth.model";
import { AuthService } from "./Auth.service";

@Controller()
export class AuthController {
    constructor(
        private httpServ: HttpService,
        private authServ: AuthService,
    ) {}
    
    private scope = "email profile"; // email
    private redirect = encodeURIComponent(process.env.GOOGLE_CLIENT_REDIRECT_URI);
    private cid = encodeURIComponent(process.env.GOOGLE_CLIENT_ID);
    // private googleRedirect = `https://accounts.google.com/o/oauth2/v2/auth?access_type=offline&response_type=code&state=login&scope=${this.scope}&redirect_uri=${this.redirect}&client_id=${this.cid}`
    private googleRedirect = `https://accounts.google.com/o/oauth2/v2/auth?access_type=offline&response_type=code&scope=${this.scope}&redirect_uri=${this.redirect}&client_id=${this.cid}`
    private cookieConfig: CookieOptions = {
        httpOnly: false, 
        secure: true,  
        maxAge: 1000 * 60 * 60 * 7, // 7 days in microseconds
        sameSite: 'strict'
    }


    // handles google redirect
    @Get('go/google')
    async go_google(@Res() res) {
        
        return res.status(301).redirect(this.googleRedirect)
    }

    @Get('google-callback')
    async google_callbak(@Res() res: Response | any, @Req() req: Request | any, @Query() query) {

        if (query.error) {
            return res.status(302).redirect('/api/go/google');
        }

        let code = decodeURIComponent(query.code);
        let r;
        try {
            r = await this.httpServ.axiosRef.post('https://oauth2.googleapis.com/token', {
                'code': code,
                'client_id': process.env.GOOGLE_CLIENT_ID,
                'client_secret': process.env.GOOGLE_CLIENT_SECRET,
                'redirect_uri': process.env.GOOGLE_CLIENT_REDIRECT_URI,
                'grant_type': 'authorization_code'
            })
        }
        catch {
            return res.status(302).redirect(this.googleRedirect)
        }
        // JWT token with the email name picture 
        let data: IGoogleUser = decode(r.data.id_token) as any;

        let user: UserDocument;
        if (!await this.authServ.findOne({email: data.email}) )
        {// user doesn't exists            
            user = await this.authServ.insertOne({email: data.email, name: data.name, _id: "", state: "", authorization: Authorization.approval})
        }
        else 
            user = await this.authServ.findOne({ email: data.email })

        console.log('Hello')

        // creates the access token and refresh token (if needed)
        let cookies = req.cookies;
        if (typeof cookies.refresh_token !== 'undefined' )
        {// has refresh token
            const [errors] = await this.authServ.validateRefreshToken(cookies.refresh_token);
            if (!errors)
            {
                return res.redirect("/")
            }

            else
            {
                let token = await this.authServ.generateRefreshToken(user);
                res.cookie('refresh_token', token, this.cookieConfig) 
            }
        }
        else 
        {// new Login 
            let token = await this.authServ.generateRefreshToken(user);
            res.cookie('refresh_token', token, this.cookieConfig)
        }
        
        return res.redirect("/")
    }

}