import { Field, ObjectType } from "@nestjs/graphql";
import { Document, Schema } from "mongoose";

export class Authorization {
    public static admin = 2;
    public static normal = 1;
    public static approval = 0;
}

export interface IUser {
    _id: String;
    name: String;
    email: String;
    authorization: number;
    state: String;
}

export const UserSchema = new Schema({
    _id: String,
    name: String,
    email: String,
    state: String,
    authorization: Number,
})

export class UserDocument extends Document {
    _id: String;
    name: String;
    email: String;
    authorization: number;
    state: String;
}

//! Other Models
export interface IGoogleUser {
    email: String,
    name: String,
}

@ObjectType()
export class UserObjectType {
    @Field()
    _id: String;

    @Field()
    name: String;

    @Field()
    email: String;

    @Field()
    accessToken: String;

    @Field(() => Number)
    authorization: Number;
}