import { Resolver, Query, Mutation, Args, Int } from "@nestjs/graphql";
import { AppService } from "src/App.service";
import { Authorization } from "src/Auth/Auth.model";
import { AuthService } from "src/Auth/Auth.service";
import { CreateOngArgs, OngOType } from "./Ong.model";
import { OngService } from "./Ong.service";

@Resolver()
export class OngResolver {
    constructor(
        private readonly ongSrv: OngService,
        private readonly authSrv: AuthService,
        private readonly appSrv: AppService,
    ){}

    @Mutation(() => OngOType, {nullable: true})
    async createOng(@Args({name: 'input', type: () => CreateOngArgs}) input: CreateOngArgs, @Args('accessToken') accessToken: string) {
        const [errors, _, user] = await this.authSrv.validateAccessToken(accessToken);
        if (!errors && user.authorization == Authorization.admin)
            return await this.ongSrv.insertOne(input);
        else 
            return null
    }

    @Mutation(() => OngOType, {nullable: true})
    async updateOng(@Args('_id') _id: String, @Args({name: 'updatedOng', type: () => CreateOngArgs}) updatedOng: CreateOngArgs, @Args('accessToken') accessToken: string) {
        console.log(`Access token: ${accessToken}`)
        const [errors, _, user] = await this.authSrv.validateAccessToken(accessToken);
        if (!errors && user.authorization == Authorization.admin)
            return await this.ongSrv.findOneAndUpdate(_id, updatedOng);
        else 
            return null
    }


    @Query(() => OngOType)
    async getOng(@Args('_id')_id: String) {
        return await this.ongSrv.findOneById(_id);
    }

    @Query(() => [OngOType])
    async getOngs(@Args({type: () => Int, name:'start'}) start: number, @Args({type: () => Int, name:'limit'}) limit: number) {
        return await this.ongSrv.findAll(start, limit);
    }

    @Query(() => [String])
    async generateIds(@Args({name: 'amount', type: () => Int}) amount: number) {
        let res = []
        for (let i = 0; i < amount; i++) {
            res = [...res, this.appSrv.genID()];
        }
        return res;
    }

    @Query(() => [OngOType])
    async search(@Args('text') text: string) {
        return this.ongSrv.search(text);
    }
}