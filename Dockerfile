FROM docker.io/node

COPY dist /app
WORKDIR /app
ENTRYPOINT [ "node", "main.js" ]