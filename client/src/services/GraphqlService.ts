import { ApolloClient, ApolloError, DocumentNode, InMemoryCache, NormalizedCacheObject } from "@apollo/client/core";
import { Readable, readable, Writable, writable } from "svelte/store";
export let client: ApolloClient<NormalizedCacheObject> = new ApolloClient({
    uri: `${window.origin}/api/gql`,
    cache: new InMemoryCache(),
})

export interface GqlData {
    loading: boolean;
	data: object | null;
	error?: ApolloError;
}

export const query = (query: DocumentNode, variables) : Writable<GqlData> => {
    let req = client.query({
        query,
        variables,
    })
    let store = writable<GqlData>({
        data: null,
        loading: true,
        error: null,
    })

    req.catch((message) => {
        store.set({
            data: null,
            loading: false,
            error: new ApolloError({errorMessage: JSON.stringify(message)})
        })
    })

    req.then((res) => {
        store.set({
            data: res.data,
            loading: false,
            error: null
        })
    })

    return store
}

export const mutate = (mutation: DocumentNode, variables: any): Readable<GqlData> => {
    let store = writable<GqlData>({
        data: null,
        loading: true,
        error: null
    })

    let req = client.mutate({
        mutation,
        variables,
    })

    req.catch((message) => {
        store.set({
            data: null,
            loading: false,
            error: new ApolloError({errorMessage: JSON.stringify(message)})
        })
    })

    req.then((res) => {
        store.set({
            data: res.data,
            loading: false,
            error: null
        })
    })

    return store;
}