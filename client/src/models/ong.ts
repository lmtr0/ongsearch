export interface IOng {
    _id?: string,
    name: string,
    introduction: string,
    cattegories: string[],
    curiosities?: string,
    site?: string,
    creationDate?: string, 
    images?: string[],
    localization?: string,
    howCanYouHelp?: string,
    contact?: string,
}